# spark_studies

Learning Spark in java/scala

gradlw run will execute the mainClassFile default defined in gradle.properties file.
To execute a class throuh gradle,  
 You can see a example below
```shell script
gradle run -PmainClassFile=br.eti.bandeira.sparkstudies.SparkElasticTest
```

You will need to DELETE the checkpoint folder  
 if you want to load again documents on ElasticSearch

Command to run a elasticSearch single-node 

```shell script
docker run -d -p 9200:9200 -e "discovery.type=single-node" -v <<LOCAL_PATH>>:/usr/share/elasticsearch/data docker.elastic.co/elasticsearch/elasticsearch:6.4.2
```

You will also need to execute a kafka on localhost