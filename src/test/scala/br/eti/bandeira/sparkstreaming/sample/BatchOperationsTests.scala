package br.eti.bandeira.sparkstreaming.sample

import org.apache.spark.sql.{SQLContext, SparkSession}
import org.apache.spark.{SparkConf, SparkContext, sql}
import org.junit.{AfterClass, BeforeClass, Test}
import org.junit.Assert.assertEquals

object BatchOperationsTests {
  var sc: SparkContext = _

  @BeforeClass
  def before(): Unit = {
    val sparkConf = new SparkConf()
      .setAppName("Test Spark Batch")
      .setMaster("local")
    sc = new SparkContext(sparkConf)
  }

  @AfterClass
  def after(): Unit = {
    sc.stop()
  }
}

class BatchOperationsTests {
  import BatchOperationsTests._
  val ss: SparkSession = SparkSession
    .builder()
    .config(sc.getConf)
    .getOrCreate()
  val sql: SQLContext = ss.sqlContext

  import  sql.implicits._

  @Test
  def testCountPairNumbers(): Unit = {
    val df = sc.parallelize(List(1, 2, 3, 4, 5, 6)).toDF()
    val count = BatchOperations.countPairs(df)
    assertEquals(3, count)
  }

  @Test
  def testSaveMultipleTypes():Unit = {
    val jsonDS = sc.parallelize(Seq("""
      { "isActive": false,
        "balance": 1431.73,
        "picture": "http://placehold.it/32x32",
        "age": 35,
        "eyeColor": "red"
      }""",
      """{
        "isActive": true,
        "balance": 2515.60,
        "picture": "http://placehold.it/32x32",
        "age": 34,
        "eyeColor": "blue"
      }""",
      """{
        "isActive": false,
        "balance": 3765.29,
        "picture": "http://placehold.it/32x32",
        "age": 26,
        "eyeColor": "blue"
      }""")
    ).toDS()
    val df = sql.read.json(jsonDS)

    val count = BatchOperations.countBlueEyes(df)
    assertEquals(count, 2)
  }
}
