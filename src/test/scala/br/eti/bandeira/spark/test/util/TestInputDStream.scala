package br.eti.bandeira.spark.test.util

import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.{StreamingContext, Time}
import org.apache.spark.streaming.dstream.InputDStream

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag

class TestInputDStream [T: ClassTag] (ssc: StreamingContext,
                                      queue: mutable.Queue[RDD[T]],
                                      defaultRDD: RDD[T]) extends InputDStream[T](ssc) {
  override def start(): Unit = {}

  override def stop(): Unit = {}

  override def compute(validTime: Time): Option[RDD[T]] = {
    val buffer = new ArrayBuffer[RDD[T]]()
    if(queue.nonEmpty) {
      Some(queue.dequeue())
    } else {
      Some(defaultRDD)
    }
  }
}
