package br.eti.bandeira.sparkstreaming.sample

import org.apache.spark.streaming.Duration
import org.apache.spark.streaming.dstream.DStream

object StreamOperations {
  def streamSum(stream: DStream[(String, Long)]): DStream[(String, Long)] =  stream.reduceByKey(_+_)

  def windowedSum(stream: DStream[(String, Long)], windowLength: Duration, slidingInterval: Duration): DStream[(String, Long)] = {
    stream.reduceByKeyAndWindow(_ + _, _ - _, windowLength, slidingInterval)
  }
}
