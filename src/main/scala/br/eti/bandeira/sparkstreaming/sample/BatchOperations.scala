package br.eti.bandeira.sparkstreaming.sample
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.udf

object BatchOperations {

  def isPair: UserDefinedFunction = udf((i:Int) => i % 2 == 0)

  def countPairs(df: DataFrame): Long = {
    df.printSchema()
    df.filter(isPair(df("value"))).count()
  }

  def countBlueEyes(df: DataFrame): Long = {
    df.filter(df("eyeColor") === "blue").count()
  }
}
