package br.eti.bandeira.sparkstudies;

import java.io.Serializable;
import java.sql.Timestamp;

public class LogEntry implements Serializable {

    private String ip;
    private String client;
    private String user;
    private String datetime;
    private String request;
    private String status;
    private String bytes;
    private String referer;
    private String agent;
    private Timestamp timestamp;

    LogEntry(String ip, String client, String user, String datetime,
             String request, String status, String bytes, String referer, String agent) {
        this.ip = ip;
        this.client = client;
        this.user = user;
        this.datetime =  datetime;
        this.request = request;
        this.status = status;
        this.bytes = bytes;
        this.referer = referer;
        this.agent = agent;
        this.timestamp = null;
    }

    public String getIp() {
        return ip;
    }

    public String getClient() {
        return client;
    }

    public String getUser() {
        return user;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getRequest() {
        return request;
    }

    public String getStatus() {
        return status;
    }

    public String getBytes() {
        return bytes;
    }

    public String getReferer() {
        return referer;
    }

    public String getAgent() {
        return agent;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setBytes(String bytes) {
        this.bytes = bytes;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
