package br.eti.bandeira.sparkstudies;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Row;
import scala.Option;
import scala.Some;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilities {

    static Pattern logPattern = Utilities.apacheLogPattern();
    static Pattern datePattern = Pattern.compile("\\[(.*?) .+]");

    /** Makes sure only ERROR messages get logged to avoid log spam. */
    public static void setupLogging()  {
        Logger.getLogger("org").setLevel(Level.ERROR);
        Logger.getLogger("akka").setLevel(Level.ERROR);
    }

    /** Retrieves a regex Pattern for parsing Apache access logs. */
    public static Pattern apacheLogPattern() {
        String ddd = "\\d{1,3}";
        String ip = String.format("(%1$s\\.%1$s\\.%1$s\\.%1$s)?",ddd);
        String client = "(\\S+)";
        String user = "(\\S+)";
        String dateTime = "(\\[.+?\\])";
        String request = "\"(.*?)\"";
        String status = "(\\d{3})";
        String bytes = "(\\S+)";
        String referer = "\"(.*?)\"";
        String agent = "\"(.*?)\"";
        String regex = String.format("%s %s %s %s %s %s %s %s %s",ip, client, user, dateTime, request, status, bytes, referer, agent);
        return Pattern.compile(regex);
    }

    // Function to convert log datetime string to Spark/SQL datetime
    public static Option<String> parseDateField(String field) throws ParseException {
        Matcher dateMatcher = datePattern.matcher(field);
        Option<String> ret = Option.apply(null);

        if (dateMatcher.find()) {
            String dateString = dateMatcher.group(1);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss", Locale.ENGLISH);
            Date date = (dateFormat.parse(dateString));
            Timestamp timestamp = new Timestamp(date.getTime());
            ret = new Some<String>(timestamp.toString());
        }
        return ret;
    }

    public static Option<Timestamp> parseTimestampField(String field) throws ParseException {
        Matcher dateMatcher = datePattern.matcher(field);
        Option<Timestamp> ret = Option.apply(null);

        if (dateMatcher.find()) {
            String dateString = dateMatcher.group(1);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss", Locale.ENGLISH);
            Date date = (dateFormat.parse(dateString));
            Timestamp timestamp = new Timestamp(date.getTime());
            ret = new Some<Timestamp>(timestamp);
        }
        return ret;
    }


    public static Option<LogEntry> parseLogOp(Row row) throws ParseException {
        Matcher matcher = logPattern.matcher(row.getString(0));
        java.util.logging.Logger log =  java.util.logging.Logger.getLogger("parseLog");
        log.setLevel(java.util.logging.Level.ALL);
        log.info("matcher.group(1)" + row.getString(0));
        if (matcher.matches()) {
            return new Some<LogEntry>(new LogEntry(
                    matcher.group(1),
                    matcher.group(2),
                    matcher.group(3),
                    parseDateField(matcher.group(4)).getOrElse(null),
                    matcher.group(5),
                    matcher.group(6),
                    matcher.group(7),
                    matcher.group(8),
                    matcher.group(9)
            ));
        }
        return Option.apply(null);
    }

    public static LogEntry parseLog(Row row) throws ParseException {
        Matcher matcher = logPattern.matcher(row.getString(0));
        java.util.logging.Logger log =  java.util.logging.Logger.getLogger("parseLog");
        log.setLevel(java.util.logging.Level.ALL);
        //log.info("matcher.group(1)" + row.getString(0));
        if (matcher.matches()) {
            LogEntry l = new LogEntry(
                    matcher.group(1),
                    matcher.group(2),
                    matcher.group(3),
                    parseDateField(matcher.group(4)).getOrElse(null),
                    matcher.group(5),
                    matcher.group(6),
                    matcher.group(7),
                    matcher.group(8),
                    matcher.group(9));
            l.setTimestamp(parseTimestampField(matcher.group(4)).getOrElse(null));
            return l;
        };
        return null;
    }
}