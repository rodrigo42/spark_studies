package br.eti.bandeira.sparkstudies;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.api.java.JavaStreamingContext;


import org.apache.log4j.Logger;
import org.apache.log4j.Level;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;
import org.spark_project.guava.collect.ImmutableList;
import org.spark_project.guava.collect.ImmutableMap;

import java.util.Map;

public class RddElasticTest {
    public static void main(String[] args) throws InterruptedException {

        Utilities.setupLogging();

        Map<String, String> optionsEs = ImmutableMap.of(
                "es.nodes.wan.only","true",
                "es.port","9200",
                "es.net.ssl","false",
                "es.nodes","192.168.99.100",
                "es.nodes.client.only", "false");

        final SparkConf sparkConf = new SparkConf().setAppName("asd").setMaster("local[2]");
        final SparkSession ss = SparkSession.builder().appName("asd").master("local[*]").getOrCreate();

        JavaSparkContext jsc = JavaSparkContext.fromSparkContext(ss.sparkContext());

        Map<String, ?> numbers = ImmutableMap.of("one", 1, "two", 2);
        Map<String, ?> airports = ImmutableMap.of("OTP", "Otopeni", "SFO", "San Fran");

        JavaRDD<Map<String, ?>> javaRDD = jsc.parallelize(ImmutableList.of(numbers, airports));

        JavaEsSpark.saveToEs(javaRDD, "spark/docs", optionsEs);
    }
}
