package br.eti.bandeira.sparkstudies;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class PureKafkaTest {
    public static void main(String[] args) {
        Properties p = new Properties();
        p.setProperty("bootstrap.servers", "localhost:9092");
        p.setProperty("group.id", "test");
        p.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        p.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(p);
        consumer.subscribe(Arrays.asList("test"));
        while (true) {
            final ConsumerRecords<String, String> poll = consumer.poll(Duration.ofSeconds(10));
            poll.forEach(record -> System.out.println(record.key() + "-" + record.value()));
        }
    }
}
