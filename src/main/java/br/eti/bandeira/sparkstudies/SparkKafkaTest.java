package br.eti.bandeira.sparkstudies;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.*;
import org.apache.spark.streaming.*;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.dstream.InputDStream;
import org.apache.spark.streaming.kafka010.*;
import scala.runtime.BoxedUnit;

import java.time.Duration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.Level;

public class SparkKafkaTest {
    public static void main(String[] args) throws InterruptedException {
        Logger.getLogger("org").setLevel(Level.OFF);
        Logger.getLogger("akka").setLevel(Level.OFF);

        Map<String, Object> p = new HashMap<>();
        p.put("bootstrap.servers", "localhost:9092");
        p.put("group.id", "test2");
        p.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        p.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        final SparkConf sparkConf = new SparkConf().setAppName("asd").setMaster("local[2]");
        final JavaStreamingContext ssc = new JavaStreamingContext(
                sparkConf,
                org.apache.spark.streaming.Duration.apply(100));
        final JavaInputDStream<ConsumerRecord<Object, Object>> test =
                KafkaUtils.createDirectStream(ssc, LocationStrategies.PreferConsistent(),
                ConsumerStrategies.Subscribe(Arrays.asList("test"), p));

        test.foreachRDD(rdd -> {
//            System.out.println("RDD:");
            rdd.foreach(x -> System.out.println(x.key() + "--" + x.value()));
        });
        ssc.start();
        ssc.awaitTermination();

    }

}
