package br.eti.bandeira.sparkstudies;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.function.ReduceFunction;
import org.apache.spark.sql.*;

import java.util.Arrays;

public class SparkTest {
    public static void main(String[] args) throws InterruptedException {
        Utilities.setupLogging();
        final SparkSession ss = SparkSession.builder().appName("asd").master("local[*]").getOrCreate();

        final Dataset<Row> csv = ss.read().csv("src/main/resources/in.csv").toDF();
        final Dataset<Row> ds = csv.withColumn("c", csv.col("_c1").cast("INT")).drop("_c1");
        ds.show();

//        csv.repartition(12).foreachPartition(x -> {
//            System.out.println(x + "->");
//            x.forEachRemaining(y -> System.out.println(y));
//            System.out.println(x + "<-");
//        });
        ds.groupBy(new Column("_c0")).max("c");
        ds.show();

    }

}
