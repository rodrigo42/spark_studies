package br.eti.bandeira.sparkstudies;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.spark_project.guava.collect.ImmutableMap;
import java.util.Collections;


public class SparkStreamingElasticTest {

    public static void main(String[] args) throws StreamingQueryException {

        Utilities.setupLogging();

        ImmutableMap<String, String> optionsEs = ImmutableMap.of(
                "es.nodes.wan.only", "true",
                "es.port", "9200",
                "es.net.ssl", "false",
                "es.nodes", "192.168.99.100",
                "es.nodes.client.only", "false");

        final SparkSession ss = SparkSession
                .builder()
                .appName("SparkStreamingES")
                .master("local[*]")
                .getOrCreate();

        // Encoders are created for Java beans
        Encoder<LogEntry> logEntryEncoder;
        logEntryEncoder = Encoders.bean(LogEntry.class);

        final Dataset<Row> rawData = ss
                .readStream()
                .text("src/main/resources/logs").toDF();

        final Dataset<LogEntry> structuredData;
        structuredData = rawData
                .flatMap((FlatMapFunction<Row, LogEntry>) x -> {
                    LogEntry ret = Utilities.parseLog(x);
                    return Collections.singletonList(ret).iterator();
                },logEntryEncoder);

        structuredData.printSchema();

        final StreamingQuery query = structuredData
                .withWatermark("timestamp","60 seconds")
                .writeStream()
                .options(optionsEs)
                .format("org.elasticsearch.spark.sql")
                .option("checkpointLocation", "e:/temp/eschk2/")
                .outputMode("append")
                .start("accesslog2/docs")
                ;
        query.awaitTermination();
        ss.stop();
    }
}
